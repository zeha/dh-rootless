dh-rootless
===========

Prototype tooling for testing a new manifest format to support
rootless builds even when static ownership is required.

See `high-level-manifest-parser.py --help` for an example of how to
run the tools.

Prerequisites
-------------


     apt install python3 python3-debian python3-yaml


Running tests
-------------

From the top level directory, run `python -m pytest`.


     dh-rootless $ python -m pytest
