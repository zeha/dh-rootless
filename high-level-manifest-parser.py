#!/usr/bin/python3

import argparse
import json
import operator
import os
import sys
import textwrap
import time

import yaml

from dh_rootless.intermediate_manifest import TarMember

try:
    from typing import Union, NoReturn, List, Tuple
except ImportError:
    pass


def must_be_file(tar_path, _):
    if not tar_path.isfile:
        raise ValueError(f"{tar_path.spec_path} must be a file but was not")


class ManifestPathInfo:
    __slots__ = ('mode',
                 'dirmode',
                 'special_mode',
                 'uid',
                 'uname',
                 'gid',
                 'gname',
                 'verifier',
                 '_used',
                 '_implicit_definition'
                 )

    def __init__(self,
                 mode=None,
                 dirmode=None,
                 special_mode=None,
                 uid=0,
                 uname='root',
                 gid=0,
                 gname='root',
                 verifier=None,
                 implicit_definition=False,
                 ):
        self.mode = mode
        self.dirmode = dirmode
        self.special_mode = special_mode
        self.uid = uid
        self.gid = gid
        self.uname = uname
        self.gname = gname
        self.verifier = verifier
        self._used = False
        self._implicit_definition = implicit_definition

    def verify(self, tar_path, tar_info):
        if self.verifier:
            self.verifier(tar_path, tar_info)

    def mark_used(self):
        self._used = True

    @property
    def has_been_used(self):
        return self._used

    @property
    def is_implicit_definition(self):
        return self._implicit_definition

    def generate_tar_member(self, tar_path: 'TarPathBase', clamp_mtime_to=None) -> TarMember:
        self._used = True
        if tar_path.is_dir:
            mode = self.dirmode
        elif tar_path.is_file:
            mode = self.mode
        else:
            mode = self.special_mode

        return TarMember.from_file(
            tar_path.tar_path,
            tar_path.fs_path,
            mode=mode,
            uid=self.uid,
            owner=self.uname,
            gid=self.gid,
            group=self.gname,
            clamp_mtime_to=clamp_mtime_to,
        )


def _normalize_path_from_spec(path):
    path = path.strip('/')
    if not path:
        raise ValueError("Cannot define ownership of /")
    if '//' in path:
        path = '/'.join(path.split('/'))
    if not path.startswith('./'):
        path = './' + path
    return path


def match_all_paths_in(path):
    path = _normalize_path_from_spec(path)
    return ('all_paths_in', path)


DEFAULT_PATH_INFO = ManifestPathInfo(dirmode=0o755, special_mode=None, implicit_definition=True)
SCRIPT_PATH_INFO = ManifestPathInfo(mode=0o0755, verifier=must_be_file, implicit_definition=True)
ETC_SUDO_PATH_INFO = ManifestPathInfo(mode=0o0440, verifier=must_be_file, implicit_definition=True)
PATH_DIR_PATH_INFO = ManifestPathInfo(mode=0o755, dirmode=0o755, special_mode=None, implicit_definition=True)

# Example built-in rules
DATA_PATH_INFO_BUILTIN = {
    match_all_paths_in('etc/init.d'): SCRIPT_PATH_INFO,
    match_all_paths_in('etc/sudoers.d'): ETC_SUDO_PATH_INFO,
}


class OwnershipDefinition:
    __slots__ = ('entity_name', 'entity_id')

    def __init__(self, entity_name, entity_id):
        self.entity_name = entity_name
        self.entity_id = entity_id


ROOT_DEFINITION = OwnershipDefinition('root', 0)

BAD_OWNER_NAMES = {'nogroup', 'nobody'}
BAD_OWNER_IDS = {65534}

OWNER_CACHE = {ROOT_DEFINITION.entity_name: ROOT_DEFINITION}
GROUP_CACHE = {ROOT_DEFINITION.entity_name: ROOT_DEFINITION}
UID_CACHE = {v.entity_id: v for v in OWNER_CACHE.values()}
GID_CACHE = {v.entity_id: v for v in GROUP_CACHE.values()}

LOADED_CACHES = False


def _load_ownership_cache():
    for owner_def in read_ownership_def_from_base_password_template('/usr/share/base-passwd/passwd.master'):
        if owner_def.entity_id == 0:
            # Leave root as it is
            continue
        OWNER_CACHE[owner_def.entity_name] = owner_def
        UID_CACHE[owner_def.entity_id] = owner_def
    for group_def in read_ownership_def_from_base_password_template('/usr/share/base-passwd/group.master'):
        if group_def.entity_id == 0:
            # Leave root as it is
            continue
        GROUP_CACHE[group_def.entity_name] = group_def
        GID_CACHE[group_def.entity_id] = group_def
    global LOADED_CACHES
    LOADED_CACHES = True


def read_ownership_def_from_base_password_template(template_file):
    with open(template_file) as fd:
        for line in fd:
            entity_name, _star, entity_id, _remainder = line.split(":", 3)
            yield OwnershipDefinition(entity_name, int(entity_id))


def parse_and_resolve_ownership(declaration, ownership_type, name_cache, id_cache):
    if declaration is None:
        return None, None
    entity_name, entity_id = _parse_ownership(declaration)
    return resolve_ownership(entity_name, entity_id, ownership_type, name_cache, id_cache)


def _parse_ownership(v):
    if isinstance(v, str) and ':' in v:
        entity_name, entity_id = v.split(':')
        entity_id = int(entity_id)
        return entity_name, entity_id

    if isinstance(v, int):
        return None, v
    return v, None


def _resolve_entity(key, key_type, cache_lookup, expected_result=None):
    try:
        resolved_entity_name = cache_lookup(key)
    except KeyError:
        raise ValueError(
            f"Refusing to use {key} as {key_type}: It is not known to be a static {key_type} from base-passwd")

    if expected_result is not None and expected_result != resolved_entity_name:
        raise ValueError(
            f"Bad {key_type} declaration: Looking up {key} resolves to {resolved_entity_name} according to base-passwd,"
            f" but the packager declared to should have been {expected_result}")
    return resolved_entity_name


def resolve_ownership(entity_name, entity_id, ownership_type, name_cache, id_cache):
    if not LOADED_CACHES:
        if (entity_name is not None and entity_name not in name_cache) or \
                (entity_id is not None and entity_id not in id_cache):
            _load_ownership_cache()
            assert LOADED_CACHES
    resolved_name = entity_name
    resolved_id = entity_id
    if entity_id is not None:
        resolved_name = _resolve_entity(entity_id,
                                        ownership_type,
                                        lambda x: id_cache[x].entity_name,
                                        expected_result=entity_name)

    if entity_name is not None:
        resolved_id = _resolve_entity(entity_name,
                                      ownership_type,
                                      lambda x: name_cache[x].entity_id,
                                      expected_result=entity_id)
    if resolved_name in BAD_OWNER_NAMES or resolved_id in BAD_OWNER_IDS:
        raise ValueError(f'Refusing to use {resolved_name}:{resolved_id} as {ownership_type}.'
                         ' No path should have this entity as owner/group as it is unsafe.')
    return resolved_name, resolved_id


def _full_key_name(key, key_prefix):
    return key if not key_prefix else key_prefix + "." + key


def _optional_key(d, key, key_prefix, manifest_path, expected_type=None, default_value=None):
    v = d.get(key)
    if v is None:
        return default_value
    if expected_type is not None:
        return _ensure_value_is_type(v, expected_type, key, key_prefix, manifest_path)
    return v


def _required_key(d, key, key_prefix, manifest_path, expected_type=None, extra=None):
    v = d.get(key)
    if v is None:
        key_path = _full_key_name(key, key_prefix)
        extra_info = " " + extra() if extra is not None else None
        _error(f'Missing required key {key_path} in manifest "{manifest_path}. {extra_info}"')

    if expected_type is not None:
        return _ensure_value_is_type(v, expected_type, key, key_prefix, manifest_path)
    return v


def _ensure_value_is_type(v, t, key, key_prefix, manifest_path):
    if v is None:
        return None
    if not isinstance(v, t):
        if isinstance(t, tuple):
            t_msg = "one of: " + ', '.join(x.__name__ for x in t)
        else:
            t_msg = f'of type {t.__name__}'
        _error(f"The key {_full_key_name(key, key_prefix)} (manifest: {manifest_path}) must be {t_msg}")
    return v


class HighLevelManifest:

    def __init__(self, manifest_path, path_specs, builtin_path_specs):
        self.manifest_path = manifest_path
        self.path_specs = path_specs
        self.builtin_path_specs = builtin_path_specs

    def _lookup(self, path):
        v = self.path_specs.get(path)
        if v is not None:
            return v
        return self.builtin_path_specs.get(path)

    def resolve_path_info(self, tar_path) -> ManifestPathInfo:
        path_info = self._lookup(tar_path.spec_path)
        if path_info is None:
            parent_dir = tar_path.spec_parent_dir
            if parent_dir is not None:
                path_info = self.builtin_path_specs.get(match_all_paths_in(tar_path.spec_parent_dir))
        if path_info is None:
            path_info = DEFAULT_PATH_INFO
        return path_info

    def validate_usage(self):
        for path, path_info in self.path_specs.items():
            if not path_info.has_been_used:
                _error(f"The path {path} in manifest {self.manifest_path} was declared but not used. If the declaration"
                       " is not needed, then please remove it. Otherwise, the path is from the deb."
                       " Note the path has been normalized and might appear slightly different in the manifest file!")

    @classmethod
    def from_dict(cls, manifest_path, d, builtin_path_specs):
        raw_path_specs = _required_key(d, 'path-metadata', '', manifest_path, expected_type=list)
        path_specs = {}
        for i, path_spec in enumerate(raw_path_specs):
            key_prefix = f'path-metadata[{i}]'
            mode = _optional_key(path_spec, 'mode', key_prefix, manifest_path, expected_type=str)
            try:
                mode = int(mode, base=8)
            except ValueError:
                key_path = _full_key_name('mode', key_prefix)
                _error(f'Unable to parse {key_path} as an octal number (manifest: {manifest_path})')
            declared_owner = _optional_key(path_spec, 'owner', key_prefix, manifest_path, expected_type=(str, int),
                                           default_value='root:0')
            declared_group = _optional_key(path_spec, 'group', key_prefix, manifest_path, expected_type=(str, int),
                                           default_value='root:0')
            paths = _required_key(path_spec, 'paths', key_prefix, manifest_path, expected_type=list)
            if not paths:
                key_path = _full_key_name('paths', key_prefix)
                _error(f'The key {key_path} must be a non-empty list of paths̈́ (manifest: {manifest_path})')

            try:
                owner, uid = parse_and_resolve_ownership(declared_owner, 'owner', OWNER_CACHE, UID_CACHE)
            except ValueError as e:
                _error(f'Invalid ownership information in {_full_key_name("owner", key_prefix)}'
                       f' (manifest: {manifest_path}): {e.args[0]}')
            try:
                group, gid = parse_and_resolve_ownership(declared_group, 'group', GROUP_CACHE, GID_CACHE)
            except ValueError as e:
                _error(f'Invalid ownership in {_full_key_name("group", key_prefix)}'
                       f' (manifest: {manifest_path}): {e.args[0]}')

            path_info = ManifestPathInfo(
                mode=mode,
                uid=uid,
                uname=owner,
                gid=gid,
                gname=group,
            )
            for path in paths:
                normalized_path = _normalize_path_from_spec(path)
                if normalized_path in path_specs:
                    _error(f'The path {path} is defined twice in manifest {manifest_path} under "path-metadata".'
                           ' Note that this duplication check is done on a normalization of the path'
                           f' (normalized path: {normalized_path})')
                path_specs[normalized_path] = path_info

        return HighLevelManifest(manifest_path, path_specs, builtin_path_specs)

    @classmethod
    def _parse_install_line(cls, manifest_path, line_no: int, install_line: List[str]) -> Tuple[
        List[str], ManifestPathInfo]:
        mode = None
        owner = 'root'
        uid = 0
        group = 'root'
        gid = 0
        seen_key_words = set()
        while install_line:
            if '=' not in install_line[-1]:
                break

            latest_entry = install_line.pop()
            k, v = latest_entry.split('=', 1)
            if k in seen_key_words:
                _error(f'The {k} keyword can only appear once per line'
                       f' (manifest: {manifest_path}:{line_no})')
            seen_key_words.add(k)
            if k == 'mode':
                mode = int(v, 8)
            elif k == 'owner':
                if ':' not in v and v != ':':
                    _error('The owner keyword must have [user]:[group] as value and at least one of [user]'
                           f' or [group] must be non-empty (manifest: {manifest_path}:{line_no})')
                declared_owner, declared_group = v.split(':')
                if declared_owner:
                    try:
                        owner, uid = parse_and_resolve_ownership(declared_owner, 'owner', OWNER_CACHE, UID_CACHE)
                    except ValueError as e:
                        _error('Invalid ownership (user) information in "owner"'
                               f' (manifest: {manifest_path}:{line_no}): {e.args[0]}')
                if declared_group:
                    try:
                        group, gid = parse_and_resolve_ownership(declared_group, 'group', GROUP_CACHE, GID_CACHE)
                    except ValueError as e:
                        _error('Invalid ownership (group) information in "owner"'
                               f' (manifest: {manifest_path}:{line_no}): {e.args[0]}')
            else:
                _error(f"Unsupported key {k} in {manifest_path} line {line_no}")

        path_info = ManifestPathInfo(
            mode=mode,
            uid=uid,
            uname=owner,
            gid=gid,
            gname=group,
        )
        return install_line, path_info

    @classmethod
    def from_install(cls, manifest_path, d: List[Tuple[int, List[str]]], builtin_path_specs):
        path_specs = {}
        for line_no, install_line in d:

            paths, path_info = cls._parse_install_line(manifest_path, line_no, install_line)
            if len(paths) != 1:
                # Cannot be bothered to do the "install foo and bar into baz" case right now.
                _error("There must be exactly one path per line in this prototype (restriction will be loosen later)."
                       f"  Bad line is {line_no} in {manifest_path}")

            # TODO: Pop the last path and use it as a target dir for true dh_install compat
            for path in paths:
                normalized_path = _normalize_path_from_spec(path)
                if normalized_path in path_specs:
                    _error(f'The path {path} is defined twice in manifest {manifest_path} under "path-metadata".'
                           ' Note that this duplication check is done on a normalization of the path'
                           f' (normalized path: {normalized_path})')
                path_specs[normalized_path] = path_info

        return HighLevelManifest(manifest_path, path_specs, builtin_path_specs)


def resolve_path_info(tar_path_obj, path_info_table=None):
    if path_info_table:
        path_info = path_info_table.get(tar_path_obj.spec_path)
        if path_info is None:
            path_info = path_info_table.get(tar_path_obj.spec_parent_dir)
        if path_info is None:
            path_info = DEFAULT_PATH_INFO
    else:
        path_info = DEFAULT_PATH_INFO
    return path_info


def parse_args():
    me = os.path.basename(sys.argv[0])
    description = textwrap.dedent(f'''\
    THIS IS A PROTOTYPE tool for testing manifest formats.
    
    Given a binary Debian package root (dpkg-deb --raw-extract some.deb) and a high-level
    manifest, it will generate a "intermediate" manifest format that `deb_packer.py` can
    consume to built a deb.

    Example usage for the prototype:

      # SETUP
      $ [ "$(whoami)" != 'root' ] || echo "There is no point if you are running this as root"
      $ TOOL_DIR=$(pwd)
      $ mkdir local-test && cd local-test
      $ apt download sudo
      $ mv sudo_*.deb original_sudo.deb
      $ dpkg-deb --raw-extract original_sudo.deb root
      $ chmod 0755 root/usr/bin/sudo
      $ echo "Verify that root/usr/bin/sudo is owned by non-root and is 0755"
      $ ls -l root/usr/bin/sudo

      # USING the prototype
      $ SOURCE_DATE_EPOCH=$(stat root/DEBIAN/ -c %Y)
      $ cat > dh-manifest.yaml <<EOF
      path-metadata:
        - mode: "04755"
          ## owner/group defaults to root.
          ## But try to uncomment for added fun
          # owner: sys
          # group: 3
          paths:
            - usr/bin/sudo
      EOF
      # (alternative format - remember to correct the --package-manifest parameter if you want to use this one)
      $ cat > install <<EOF
      # Try adding owner=sys:sys
      usr/bin/sudo mode=04755
      EOF
      $ SOURCE_DATE_EPOCH=$SOURCE_DATE_EPOCH "$TOOL_DIR/{me}" --package-manifest dh-manifest.yaml root intermediate-manifest.json
      $ SOURCE_DATE_EPOCH=$SOURCE_DATE_EPOCH "$TOOL_DIR/deb_packer.py" --intermediate-package-manifest intermediate-manifest.json root .
      $ mv sudo_*.deb repacked_sudo.deb

      # Verify that usr/bin/sudo is now setuid and owned by root (or whatever owner you specified in the manifest)
      $ diffoscope original_sudo.deb repacked_sudo.deb
    ''')

    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument('package_root_dir', metavar="PACKAGE_ROOT_DIR",
                        help='Root directory of the package. Must contain a DEBIAN directory'
                        )
    parser.add_argument('intermediate_manifest_output', metavar="PATH",
                        help='Path where the intermediate manifest should be placed.'
                        )

    parser.add_argument('--package-manifest', dest='package_manifest',
                        action='store', default=None,
                        help='Manifest of file metadata that is not visible in the file-system')
    parser.add_argument('--source-date-epoch', dest='source_date_epoch',
                        action='store', type=int, default=None,
                        help='Source date epoch (can also be given via the SOURCE_DATE_EPOCH environ variable'
                        )

    parsed_args = parser.parse_args()

    return parsed_args


def _error(msg) -> 'NoReturn':
    me = os.path.basename(sys.argv[0])
    print(f"{me}: error: {msg}", file=sys.stderr)
    sys.exit(1)


def parse_manifest(parsed_args) -> HighLevelManifest:
    manifest_path = parsed_args.package_manifest
    if manifest_path is None:
        return HighLevelManifest('<built-in, data.tar>', {}, DATA_PATH_INFO_BUILTIN)
    if manifest_path.endswith(".yaml"):
        with open(manifest_path) as fd:
            data = yaml.safe_load(fd)
            return HighLevelManifest.from_dict(manifest_path, data, DATA_PATH_INFO_BUILTIN)
    if (manifest_path.endswith(('/install', '.install')) or manifest_path == 'install' or
            manifest_path.endswith(('/install-manifest', '.install-manifest')) or manifest_path == 'install-manifest'):
        with open(manifest_path) as fd:
            data = [(no, line.split())
                    for no, line in enumerate(fd, start=1) if line.strip() and not line.strip().startswith("#")
                    ]
            return HighLevelManifest.from_install(manifest_path, data, DATA_PATH_INFO_BUILTIN)
    _error("The manifest path must be either a .yaml or a debhelper .install file")


def generate_intermediate_manifest(root_dir: str, manifest: HighLevelManifest, clamp_mtime_to: int) -> List[TarMember]:
    members = []
    for tar_path_obj in find_paths_in_order(root_dir):
        path_info = manifest.resolve_path_info(tar_path_obj)
        tar_member = path_info.generate_tar_member(tar_path_obj, clamp_mtime_to=clamp_mtime_to)
        members.append(tar_member)
    return members


def output_intermediate_manifest(manifest_output_file: str, members: List[TarMember]) -> None:
    with open(manifest_output_file, 'w') as fd:
        serial_format = [m.to_manifest() for m in members]
        json.dump(serial_format, fd)


def main():
    parsed_args = parse_args()
    root_dir = parsed_args.package_root_dir
    output_path = parsed_args.intermediate_manifest_output

    mtime = parsed_args.source_date_epoch
    if mtime is None and 'SOURCE_DATE_EPOCH' in os.environ:
        mtime = int(os.environ['SOURCE_DATE_EPOCH'])
    if mtime is None:
        mtime = int(time.time())

    manifest = parse_manifest(parsed_args)
    intermediate_manifest = generate_intermediate_manifest(root_dir, manifest, mtime)
    manifest.validate_usage()
    output_intermediate_manifest(output_path, intermediate_manifest)


class TarPathBase:
    __slots__ = ['_spec_path', '_tar_path', '_fs_path', '_tar_parent_dir']

    def __init__(self, spec_path, tar_path, fs_path, tar_parent_dir):
        self._spec_path = spec_path
        self._tar_path = tar_path
        self._fs_path = fs_path
        self._tar_parent_dir = tar_parent_dir

    @property
    def fs_path(self):
        return self._fs_path

    @property
    def tar_path(self):
        return self._tar_path

    @property
    def spec_path(self):
        return self._spec_path

    @property
    def spec_parent_dir(self):
        return None if self._spec_path == '.' else os.path.dirname(self._spec_path)

    @property
    def is_dir(self):
        raise NotImplementedError()

    @property
    def is_file(self):
        raise NotImplementedError()

    @property
    def is_symlink(self):
        raise NotImplementedError()


class TarRootDir(TarPathBase):

    @property
    def is_dir(self):
        return True

    @property
    def is_file(self):
        return False

    @property
    def is_symlink(self):
        return False


class TarPathDirEntry(TarPathBase):
    __slots__ = ('_dir_entry',)

    def __init__(self, spec_path, fs_path, tar_parent_dir, dir_entry):
        super().__init__(
            spec_path,
            spec_path + '/' if dir_entry.is_dir(follow_symlinks=False) else spec_path,
            fs_path,
            tar_parent_dir,
        )
        self._dir_entry = dir_entry

    @property
    def is_dir(self):
        return self._dir_entry.is_dir(follow_symlinks=False)

    @property
    def is_file(self):
        return self._dir_entry.is_file(follow_symlinks=False)

    @property
    def is_symlink(self):
        return self._dir_entry.is_symlink()


BY_NAME = operator.attrgetter('name')


def _scan_dir(tar_path, fs_dir):
    child_paths = sorted((p for p in os.scandir(fs_dir) if p.name not in ('.', '..')), key=BY_NAME, reverse=True)
    yield from (TarPathDirEntry(os.path.join(tar_path, p.name), os.path.join(fs_dir, p.name), tar_path, p)
                for p in child_paths)


def find_paths_in_order(root_dir):
    # We cannot use os.walk as it does not produce the right order. We wanted "sorted DFS (dir/files interleaved,
    # but symlinks last)" and os.walk can give use "unsorted DFS (child first)" or "sorted BFS (files first,
    # then dirs)" - neither of which is what we need.
    #
    # Test with:
    #
    #  etc/
    #  etc/apache2/
    #  etc/apache2/...
    #  etc/cgitrc  (<-- non-directory)
    #  etc/... (<-- any non-symlink file type here as long as it sort after cgitrc)
    #  ...
    #  <... followed by all symlinks ordered as above ...>
    #
    # The listed order is the correct order for emulating dpkg-deb but os.walk does not enable us to generate
    # that order.
    #
    symlinks = []
    stack = [TarRootDir('.', '.', root_dir, '.')]
    while stack:
        current = stack.pop()
        if current.is_symlink:
            symlinks.append(current)
            continue
        yield current
        if not current.is_dir:
            continue
        child_paths = _scan_dir(current.tar_path, current.fs_path)
        if current.tar_path == '.':
            child_paths = (t for t in child_paths if t.spec_path != './DEBIAN')
        stack.extend(child_paths)
    yield from symlinks


if __name__ == '__main__':
    main()
