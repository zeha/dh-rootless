import argparse
import json
from pathlib import Path

import pytest

import deb_packer


def write_unpacked_deb(root: Path, package: str, version: str, arch: str):
    debian = root / 'DEBIAN'
    debian.mkdir(mode=0o755)
    (debian / 'control').write_text(f"Package: {package}\nVersion: {version}\nArchitecture: {arch}\n")


def test_pack_smoke(tmp_path):
    mtime = 1668973695

    root_dir = tmp_path / 'root'
    root_dir.mkdir()
    write_unpacked_deb(root_dir, "fake", "1.0", "amd64")
    output_path = tmp_path / 'out'
    output_path.mkdir()
    deb_file = Path(output_path) / 'output.deb'

    parsed_args = argparse.Namespace(is_udeb=False, compression_level=None, compression_strategy=None)

    data_compression = deb_packer.COMPRESSIONS['xz']
    data_compression_cmd = data_compression.as_cmdline(parsed_args)
    ctrl_compression = data_compression
    ctrl_compression_cmd = data_compression_cmd

    package_manifest = tmp_path / 'temporary-manifest.json'
    package_manifest.write_text(json.dumps({}))

    deb_packer.pack(str(deb_file), ctrl_compression, data_compression, str(root_dir),
                    str(package_manifest), mtime, ctrl_compression_cmd, data_compression_cmd)

    binary = deb_file.read_bytes()

    assert binary == (
        b'!<arch>\n'
        b'debian-binary   1668973695  0     0     100644  4         `\n'
        b'2.0\n'
        b'control.tar.xz  1668973695  0     0     100644  244       `\n'
        b'\xfd7zXZ\x00\x00\x04\xe6\xd6\xb4F\x04\xc0\xb4\x01\x80P!\x01\x16\x00\x00\x00'
        b"\x00\x00\x00\x00\x19\x87 E\xe0'\xff\x00\xac]\x00\x17\x0b\xbc\x1c}"
        b'\x01\x95\xc0\x1dJ>y\x15\xc2\xcc&\xa3^\x11\xb5\x81\xa6\x8cI\xd2\xf0m\xdd\x04'
        b'M\xb2|Tdy\xf5\x00H\xab\xa6B\x11\x8d2\x0e\x1d\xf8F\x9e\x9a\xb0\xb8_]\xa3;M'
        b"t\x90\x9a\xe3)\xeb\xadF\xfet'b\x05\x85\xd5\x04g\x7f\x89\xeb=(\xfd\xf6"
        b'"p\xc3\x91\xf2\xd3\xd2\xb3\xed%i\x9a\xfa\\\xde7\xd5\x01\x18I\x14D\x10E'
        b'\xba\xdf\xfb\x12{\x84\xc4\x10\x08,\xbc\x9e\xac+w\x07\r`|\xcfFL#\xbb'
        b'S\x91\xb4\\\x9b\x80&\x1d\x9ej\x13\xe3\x13\x02=\xe9\xd5\xcf\xb0\xdf?L\xf1\x96'
        b'\xd2\xc6bh\x19|?\xc2j\xe58If\xb7Y\xb9\x18:\x00\x00|\xfb\xcf\x82e/\xd05'
        b'\x00\x01\xd0\x01\x80P\x00\x00\xc9y\xeem\xb1\xc4g\xfb\x02\x00\x00\x00'
        b'\x00\x04YZ'
        b'data.tar.xz     1668973695  0     0     100644  116       `\n'
        b'\xfd7zXZ\x00\x00\x04\xe6\xd6\xb4F\x04\xc04\x80P!\x01\x16\x00\x00\x00\x00'
        b"\x00\x00\x00\x00r8\x97\xe5\xe0'\xff\x00,]\x00\x00o\xfd\xff\xff\xa3\xb7\xffG"
        b'>H\x15r9aQ\xb8\x92(\xe6\xa3\x86\x07\xf9\xee\xe4\x1e\x82\xd3/\xc5:<\x01K\xb1~'
        b'\xc9\x8a\\2\x1bd\x00\x00\xaf\x18\x12\xb3/U\xcc3\x00\x01P\x80P\x00\x00\x00'
        b'\x1ac0\xb5\xb1\xc4g\xfb\x02\x00\x00\x00\x00\x04YZ'
    )


@pytest.mark.parametrize("package,version,arch,is_udeb,expected", [
    ("fake", "1.0", "amd64", False, "fake_1.0_amd64.deb"),
    ("fake", "1.0", "amd64", True, "fake_1.0_amd64.udeb"),
    ("fake", "2:1.0", "amd64", False, "fake_1.0_amd64.deb"),
    ("fake", "2:1.0", "amd64", True, "fake_1.0_amd64.udeb"),
    ("fake", "3.0", "all", False, "fake_3.0_all.deb"),
    ("fake", "3.0", "all", True, "fake_3.0_all.udeb"),
])
def test_generate_deb_filename(tmp_path, package, version, arch, is_udeb, expected):
    write_unpacked_deb(tmp_path, package, version, arch)
    assert deb_packer._compute_output_filename(str(tmp_path), is_udeb) == expected
