#!/usr/bin/python3

import argparse
import io
import json
import operator
import os
import subprocess
import sys
import tarfile
import textwrap
import time

from debian.deb822 import Deb822

from dh_rootless.intermediate_manifest import TarMember, PathType

try:
    from typing import Union, NoReturn, Optional, List, FrozenSet, Iterable, IO
except ImportError:
    pass

# AR header / start of a deb file for reference
# 00000000  21 3c 61 72 63 68 3e 0a  64 65 62 69 61 6e 2d 62  |!<arch>.debian-b|
# 00000010  69 6e 61 72 79 20 20 20  31 36 36 38 39 37 33 36  |inary   16689736|
# 00000020  39 35 20 20 30 20 20 20  20 20 30 20 20 20 20 20  |95  0     0     |
# 00000030  31 30 30 36 34 34 20 20  34 20 20 20 20 20 20 20  |100644  4       |
# 00000040  20 20 60 0a 32 2e 30 0a  63 6f 6e 74 72 6f 6c 2e  |  `.2.0.control.|
# 00000050  74 61 72 2e 78 7a 20 20  31 36 36 38 39 37 33 36  |tar.xz  16689736|
# 00000060  39 35 20 20 30 20 20 20  20 20 30 20 20 20 20 20  |95  0     0     |
# 00000070  31 30 30 36 34 34 20 20  39 33 36 38 20 20 20 20  |100644  9368    |
# 00000080  20 20 60 0a fd 37 7a 58  5a 00 00 04 e6 d6 b4 46  |  `..7zXZ......F|


class ArMember:

    def __init__(self, name, mtime, fixed_binary=None, write_to_impl=None):
        self.name = name
        self._mtime = mtime
        self._write_to_impl = write_to_impl
        self.fixed_binary = fixed_binary

    @property
    def is_fixed_binary(self):
        return self.fixed_binary is not None

    @property
    def mtime(self):
        return self.mtime

    def write_to(self, fd):
        self._write_to_impl(fd)


AR_HEADER_LEN = 60
AR_HEADER = b' ' * AR_HEADER_LEN


def write_header(fd: IO[bytes], member: ArMember, member_len: int, mtime: int) -> None:
    header = b'%-16s%-12d0     0     100644  %-10d\x60\n' % (member.name.encode('ascii'), mtime, member_len)
    fd.write(header)


def generate_ar_archive(output_filename: str, mtime: int, members: Iterable[ArMember]):
    with open(output_filename, 'wb', buffering=0) as fd:
        fd.write(b'!<arch>\n')
        for member in members:
            if member.is_fixed_binary:
                write_header(fd, member, len(member.fixed_binary), mtime)
                fd.write(member.fixed_binary)
            else:
                header_pos = fd.tell()
                fd.write(AR_HEADER)
                member.write_to(fd)
                current_pos = fd.tell()
                fd.seek(header_pos, os.SEEK_SET)
                content_len = current_pos - header_pos - AR_HEADER_LEN
                assert content_len >= 0
                write_header(fd, member, content_len, mtime)
                fd.seek(current_pos, os.SEEK_SET)
    print(f"Generated {output_filename}")


def _generate_tar_file(tar_members: List[TarMember], compression_cmd, write_to: io.BufferedWriter):
    with (
            subprocess.Popen(compression_cmd, stdin=subprocess.PIPE, stdout=write_to) as compress_proc,
            tarfile.open(mode='w|', fileobj=compress_proc.stdin, format=tarfile.GNU_FORMAT, errorlevel=1) as tar_fd,
         ):
        for tar_member in tar_members:
            tar_info: tarfile.TarInfo = tar_member.create_tar_info(tar_fd)
            if tar_info is None:
                assert tar_member.only_if_present
                continue
            if tar_member.path_type == PathType.FILE:
                with open(tar_member.fs_path, 'rb') as mfd:
                    tar_fd.addfile(tar_info, fileobj=mfd)
            else:
                tar_fd.addfile(tar_info)
    compress_proc.wait()
    if compress_proc.returncode != 0:
        _error(f"Compression command {compression_cmd} failed with code {compress_proc.returncode}")


def generate_tar_file_member(tar_members, compression_cmd):
    def _impl(fd):
        return _generate_tar_file(tar_members,
                                  compression_cmd,
                                  fd,
                                  )
    return _impl


def _error(msg) -> 'NoReturn':
    me = os.path.basename(sys.argv[0])
    print(f"{me}: error: {msg}", file=sys.stderr)
    sys.exit(1)


def _xz_cmdline(compression_rule: 'Compression', parsed_args):
    compression_level = compression_rule.effective_compression_level(parsed_args)
    cmdline = ['xz', '-T2', '-' + str(compression_level)]
    strategy = None if parsed_args is None else parsed_args.compression_strategy
    if strategy is None:
        strategy = 'extreme' if parsed_args.is_udeb else 'none'
    if strategy != "none":
        cmdline.append('-S' + strategy)
    return cmdline


def _gzip_cmdline(compression_rule: 'Compression', parsed_args):
    compression_level = compression_rule.effective_compression_level(parsed_args)
    cmdline = ['gzip', '-n' + str(compression_level)]
    strategy = None if parsed_args is None else parsed_args.compression_strategy
    if strategy is not None and strategy != 'none':
        raise ValueError(f'Not implemented: Compression strategy {strategy}'
                         ' for gzip is currently unsupported (but dpkg-deb does)')
    return cmdline


def _uncompressed_cmdline(_unused_a: 'Compression', _unused_b):
    return ['cat']


class Compression:

    def __init__(self,
                 default_compression_level: int,
                 extension: str,
                 allowed_strategies: FrozenSet[str],
                 cmdline_builder
                 ):
        self.default_compression_level = default_compression_level
        self.extension = extension
        self.allowed_strategies = allowed_strategies
        self.cmdline_builder = cmdline_builder

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} {self.extension}>'

    def effective_compression_level(self, parsed_args) -> int:
        if parsed_args and parsed_args.compression_level is not None:
            return parsed_args.compression_level
        return self.default_compression_level

    def as_cmdline(self, parsed_args) -> List[str]:
        return self.cmdline_builder(self, parsed_args)

    def with_extension(self, filename: str) -> str:
        return filename + self.extension


COMPRESSIONS = {
    'xz': Compression(6, '.xz', frozenset({'none', 'extreme'}), _xz_cmdline),
    'gzip': Compression(9, '.gz', frozenset({'none', 'filtered', 'huffman', 'rle', 'fixed'}), _gzip_cmdline),
    'none': Compression(0, '', frozenset({'none'}), _uncompressed_cmdline),
}


def _normalize_compression_args(parsed_args):
    if parsed_args.compression_level == 0 and parsed_args.compression_algorithm == 'gzip':
        print("Note: Mapping compression algorithm to none for compatibility with dpkg-deb (due to -Zgzip -z0)")
        setattr(parsed_args, 'compression_algorithm', 'none')

    compression = COMPRESSIONS[parsed_args.compression_algorithm]
    strategy = parsed_args.compression_strategy
    if strategy is not None and strategy not in compression.allowed_strategies:
        _error(f'Compression algorithm "{parsed_args.compression_algorithm}" does not support compression strategy'
               f' "{strategy}".  Allowed values: {", ".join(sorted(compression.allowed_strategies))}')
    return parsed_args


def _compute_output_filename(package_root_dir: str, is_udeb: bool) -> str:
    with open(os.path.join(package_root_dir, 'DEBIAN', 'control'), 'rt') as fd:
        control_file = Deb822(fd)

    package_name = control_file['Package']
    package_version = control_file['Version']
    package_architecture = control_file['Architecture']
    extension = control_file.get('Package-Type')
    if ':' in package_version:
        package_version = package_version.split(':', 1)[1]
    if extension is None:
        extension = 'deb'
    if is_udeb:
        extension = 'udeb'

    return f'{package_name}_{package_version}_{package_architecture}.{extension}'


def parse_args():
    description = textwrap.dedent(f'''\
    THIS IS A PROTOTYPE "dpkg-deb -b" emulator with basic manifest support

    DO NOT USE THIS TOOL DIRECTLY.  It has not stability guarantees and will be removed as
    soon as "dpkg-deb -b" grows support for the relevant features. 

    This tool is a prototype "dpkg-deb -b"-like interface for compiling a Debian package
    without requiring root even for static ownership.  It is a temporary stand-in for
    "dpkg-deb -b" until "dpkg-deb -b" will get support for a manifest.

    The tool operates on an internal JSON based manifest for now, because it was faster
    than building an mtree parser (which is the format that dpkg will likely end up
    using).
    
    As the tool is not meant to be used directly, it is full of annoying paper cuts that
    I refuse to fix or maintain. Use the high level tool instead.

    ''')

    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument('package_root_dir', metavar="PACKAGE_ROOT_DIR",
                        help='Root directory of the package. Must contain a DEBIAN directory'
                        )
    parser.add_argument('package_output_path', metavar="PATH",
                        help='Path where the package should be placed.  If it is directory,'
                             ' the base name will be determined from the package metadata'
                        )

    parser.add_argument('--udeb', dest='is_udeb',
                        action='store_true', default=False,
                        help='The package being built is a udeb.'
                        )

    parser.add_argument('--intermediate-package-manifest', dest='package_manifest', metavar="JSON_FILE",
                        action='store', default=None,
                        help='INTERMEDIATE package manifest (JSON!)')
    parser.add_argument('--root-owner-group', dest='root_owner_group', action='store_true',
                        help='Ignored. Accepted for compatibility with dpkg-deb -b')
    parser.add_argument('-b', '--build', dest='build_param', action='store_true',
                        help='Ignored. Accepted for compatibility with dpkg-deb')
    parser.add_argument('--source-date-epoch', dest='source_date_epoch',
                        action='store', type=int, default=None,
                        help='Source date epoch (can also be given via the SOURCE_DATE_EPOCH environ variable'
                        )
    parser.add_argument('-Z', dest='compression_algorithm', choices=COMPRESSIONS, default='xz',
                        help='The compression algorithm to be used'
                        )
    parser.add_argument('-z', dest='compression_level', metavar='{0-9}',
                        choices=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                        default=None,
                        type=int, help='The compression level to be used'
                        )
    parser.add_argument('-S', dest='compression_strategy',
                        # We have a different default for xz when strategy is unset and we are building a udeb
                        action="store", default=None,
                        help='The compression algorithm to be used. Concrete values depend on the compression'
                             ' algorithm, but the value "none" is always allowed'
                        )
    parser.add_argument('--uniform-compression', dest='uniform_compression',
                        action="store_true", default=True,
                        help='Whether to use the same compression for the control.tar and the data.tar.'
                             ' The default is to use uniform compression.'

                        )
    parser.add_argument('--no-uniform-compression', dest='uniform_compression',
                        action="store_false", default=True,
                        help='Disable uniform compression (see --uniform-compression)'
                        )

    parsed_args = parser.parse_args()
    parsed_args = _normalize_compression_args(parsed_args)

    return parsed_args


def _ctrl_member(member_path, fs_path=None, path_type=PathType.FILE, mode=0o644, mtime=0, only_if_present=True):
    if fs_path is None:
        assert member_path.startswith('./')
        fs_path = 'DEBIAN' + member_path[1:]
    return TarMember(
        member_path=member_path,
        path_type=path_type,
        fs_path=fs_path,
        mode=mode,
        owner='root',
        uid=0,
        group='root',
        gid=0,
        mtime=mtime,
        only_if_present=only_if_present,
    )


# TODO: Custom members should be auto-detected, but meh. Hopefully dpkg will be ready before a new ctrl member shows up.
CTRL_TAR_MEMBER_TEMPLATE = sorted([
    _ctrl_member('.', 'DEBIAN', path_type=PathType.DIRECTORY, mode=0o755, only_if_present=False),
    _ctrl_member('./control'),
    _ctrl_member('./conffiles'),
    _ctrl_member('./md5sums'),
    _ctrl_member('./shlibs'),
    _ctrl_member('./symbols'),
    _ctrl_member('./triggers'),
    _ctrl_member('./postinst', mode=0o0755),
    _ctrl_member('./preinst', mode=0o0755),
    _ctrl_member('./prerm', mode=0o0755),
    _ctrl_member('./postrm', mode=0o0755),
    _ctrl_member('./config', mode=0o0755),
    _ctrl_member('./isinstallable', mode=0o0755),
], key=operator.attrgetter('member_path'))


def _ctrl_tar_members(package_root_dir: str, mtime: int) -> List[TarMember]:
    return [
        _ctrl_member(
            cm.member_path,
            path_type=cm.path_type,
            fs_path=os.path.join(package_root_dir, cm.fs_path),
            mode=cm.mode,
            mtime=mtime,
            only_if_present=cm.only_if_present,
        )
        for cm in CTRL_TAR_MEMBER_TEMPLATE
    ]


def parse_manifest(manifest_path: 'Optional[str]') -> 'List[TarMember]':
    if manifest_path is None:
        _error(f'--intermediate-package-manifest is mandatory for now')
    with open(manifest_path) as fd:
        data = json.load(fd)
        return [TarMember.from_dict(m) for m in data]


def main():
    parsed_args = parse_args()
    root_dir: str = parsed_args.package_root_dir
    output_path: str = parsed_args.package_output_path

    mtime: int = parsed_args.source_date_epoch
    if mtime is None and 'SOURCE_DATE_EPOCH' in os.environ:
        mtime = int(os.environ['SOURCE_DATE_EPOCH'])
    if mtime is None:
        mtime = int(time.time())

    data_compression: Compression = COMPRESSIONS[parsed_args.compression_algorithm]
    data_compression_cmd = data_compression.as_cmdline(parsed_args)
    if parsed_args.uniform_compression:
        ctrl_compression = data_compression
        ctrl_compression_cmd = data_compression_cmd
    else:
        ctrl_compression = COMPRESSIONS['gzip']
        ctrl_compression_cmd = COMPRESSIONS['gzip'].as_cmdline(None)

    if output_path.endswith('/') or os.path.isdir(output_path):
        deb_file = os.path.join(output_path, _compute_output_filename(root_dir, parsed_args.is_udeb))
    else:
        deb_file = output_path

    pack(deb_file, ctrl_compression, data_compression, root_dir, parsed_args.package_manifest,
         mtime, ctrl_compression_cmd, data_compression_cmd)


def pack(deb_file: str, ctrl_compression: Compression, data_compression: Compression, root_dir: str,
         package_manifest: 'Optional[str]', mtime: int, ctrl_compression_cmd, data_compression_cmd):

    data_tar_members = parse_manifest(package_manifest)
    members = [
        ArMember('debian-binary', mtime, fixed_binary=b'2.0\n'),
        ArMember(ctrl_compression.with_extension('control.tar'), mtime,
                 write_to_impl=generate_tar_file_member(
                     _ctrl_tar_members(root_dir, mtime),
                     ctrl_compression_cmd
                 )),
        ArMember(data_compression.with_extension('data.tar'), mtime,
                 write_to_impl=generate_tar_file_member(
                     data_tar_members,
                     data_compression_cmd
                 )),
    ]
    generate_ar_archive(deb_file, mtime, members)


if __name__ == '__main__':
    main()
