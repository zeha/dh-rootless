import dataclasses
import os
import stat
import tarfile
from enum import Enum


try:
    from typing import Union, NoReturn, Optional, List, FrozenSet, Iterable, IO, Dict, Any
except ImportError:
    pass


class PathType(Enum):
    FILE = ('file', tarfile.REGTYPE)
    DIRECTORY = ('directory', tarfile.DIRTYPE)
    SYMLINK = ('symlink', tarfile.SYMTYPE)
    # TODO: Add hardlink, FIFO, Char device, BLK device, etc.

    @property
    def manifest_key(self) -> str:
        return self.value[0]

    @property
    def tarinfo_type(self):
        return self.value[1]


KEY2PATH_TYPE = {
    pt.manifest_key: pt for pt in PathType
}


@dataclasses.dataclass
class TarMember:

    member_path: str
    path_type: PathType
    fs_path: str
    mode: int
    owner: str
    uid: int
    group: str
    gid: int
    mtime: int
    only_if_present: bool
    # link_target: Optional[str]

    def create_tar_info(self, tar_fd: tarfile.TarFile) -> Union[tarfile.TarInfo, None]:
        # Special-case for built-in rules for the control.tar, where we define all files even when they do not
        # exist.
        if self.only_if_present and not os.path.exists(self.fs_path):
            return None
        # TODO: Handle cases where the file does not exist on the file system (e.g., devices)
        tar_info: tarfile.TarInfo = tar_fd.gettarinfo(name=self.fs_path, arcname=self.member_path)
        tar_info.mode = self.mode
        tar_info.uname = self.owner
        tar_info.uid = self.uid
        tar_info.gname = self.group
        tar_info.gid = self.gid
        tar_info.mode = self.mode
        tar_info.mtime = self.mtime

        return tar_info

    @classmethod
    def from_file(cls, member_path, fs_path, mode=None, owner='root', uid=0, group='root', gid=0, clamp_mtime_to=None):
        st_result = os.lstat(fs_path)
        st_mode = st_result.st_mode
        if stat.S_ISREG(st_mode):
            path_type = PathType.FILE
        elif stat.S_ISDIR(st_mode):
            path_type = PathType.DIRECTORY
#        elif stat.S_ISFIFO(st_result):
#            type = FIFOTYPE
        elif stat.S_ISLNK(st_mode):
            path_type = PathType.SYMLINK
#        elif stat.S_ISCHR(st_result):
#            type = CHRTYPE
#        elif stat.S_ISBLK(st_result):
#            type = BLKTYPE
        else:
            raise ValueError(f"The path {fs_path} had an unsupported/unknown file type. Probably a bug in the tool")

        if mode is None:
            mode = stat.S_IMODE(st_mode)
        mtime = st_result.st_mtime
        if clamp_mtime_to is not None and mtime > clamp_mtime_to:
            mtime = clamp_mtime_to

        return cls(
            member_path=member_path,
            path_type=path_type,
            fs_path=fs_path,
            mode=mode,
            owner=owner,
            uid=uid,
            group=group,
            gid=gid,
            mtime=int(mtime),
            # Internal only for the control tar
            only_if_present=False,
        )

    def to_manifest(self) -> Dict[str, Any]:
        d = dataclasses.asdict(self)
        d['mode'] = oct(self.mode)
        d['path_type'] = self.path_type.manifest_key
        del d['only_if_present']
        return d

    @classmethod
    def from_dict(cls, d) -> 'TarMember':
        mode = d['mode']
        if not mode.startswith('0o'):
            raise ValueError(f"Bad mode for {d['member_path']}")
        return cls(
            member_path=d['member_path'],
            path_type=KEY2PATH_TYPE[d['path_type']],
            fs_path=d['fs_path'],
            mode=int(mode[2:], 8),
            owner=d['owner'],
            uid=d['uid'],
            group=d['group'],
            gid=d['gid'],
            mtime=d['mtime'],
            # Internal only for the control tar
            only_if_present=False,
        )
